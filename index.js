
const http = require('http');

function checkin() {
    http.get({
        host: process.argv[2],
        port: process.argv[3] || 18080,
        path: '/checkin',
        json: true
    }, (response) => {
        if (response.statusCode != 200) {
            console.error(`Got unexpected status from registry server: ${response.statusCode} ${response.statusMessage}`);
        } else {
            console.error(`Checked in with registry server.`);
        }
    }).on('error', function(err) {
        // handle errors with the request itself
        console.error('Error with the request:', err.message);
    });

    if (process.argv[4]) {
        setTimeout(checkin, 10000);
    }
}

checkin();
